# Developer Cheat Sheet

## Build

Generates a Docker image of the ReportingIntegration server.

**Use this command if you do not yet have a Docker image or you have modified files in `src`.**

```bash
bin/build.sh
```

## Lint

Check all files meet standards. This command runs locally all linters that
will run in the pipeline on a push.

```bash
commands/up.sh
```

## Down

Removes the Docker containers and networks. The database will be emptied of all data.

```bash
commands/down.sh
```

## Rebuild

Takes down Docker containers and networks, rebuilds the Docker image for the server, and starts the Docker containers and networks.
<!--alex ignore-->
**Use this command if you have modified files in `src` and want to restart the local server.** If you have not modified files in `src` and simply want to restart the server, use `commands/restart.sh` (see below).

This will continue to run, and generate logging messages, in your terminal until it is terminated with Ctrl+C.

```bash
commands/rebuild.sh
```

## Restart

Takes down Docker containers and networks and restarts the Docker containers and networks.

**Use this command if you have not modified files in `src` and want to restart the local server.** If you have modified files in `src` and want to restart the server, use `commands/rebuild.sh` (see above).

This will continue to run, and generate logging messages, in your terminal until it is terminated with Ctrl+C.

```bash
commands/restart.sh
```

## Backend Data

Loads some sample data into the backend server.

```bash
commands/backend-data.sh
```

## Squash commits to prepare for merge into main

Squash commits into a single commit with a conventional commit message.

Use the following command before merging a merge request:

```bash
bin/premerge-squash.sh
```

## Prepare Release

Generate release artifacts for a specified version.

This script takes a version number as an argument and prepares release artifacts
for that version. It creates a directory structure under `./artifacts/release`
and generates an OpenAPI specification file named `openapi.yaml` with the
specified version number. The script performs a search and replace operation
within the original OpenAPI specification file to update the version number
to match the provided version.

```bash
bin/prepare-release.sh <version>
```
