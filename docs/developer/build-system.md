# Build System

The build system is responsible for building the products
of this project, which is a Docker image of the GuestInfoFrontend server.

```bash
bin/build.sh
```
<!-- markdownlint-disable MD013 -->
This runs `bin/build.sh` which generates a Docker image of the GenerateWCFBReportFrontend server.
